<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('anonymous');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }

    /**
     * Registration anonymous user for autoregistration.
     *
     * @return \Illuminate\Http\Response
     */
    public function registerAnonymous()
    {
        $lastUser = User::orderBy('id', 'desc')->first();
        if ($lastUser) {
            $lastId = $lastUser->id + 1;
        } else {
            $lastId = 1;
        }

        $data = [
            'name' => "Anonymous User {$lastId}",
            'email' => "anonymous.user.{$lastId}@solvelution.nz",
            'password' => str_random(10),
        ];

        $user = $this->create($data);

        //in case there was a gap in id-s
        $user->name = "Anonymous User {$user->id}";
        $user->email = "anonymous.user.{$user->id}@solvelution.nz";
        $user->save();

        $this->guard()->login($user, true);

        redirect($this->redirectPath());
    }

    /**
     * Handle a registration request for (update anonymous user)
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        $request['password'] = bcrypt($request['password']);

        $user = Auth::user();
        $user->update($request->all());
        $user->is_registered = 1;
        $user->save();

        event(new Registered($user));

        $this->guard()->login($user);

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }
}
