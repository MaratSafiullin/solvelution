<?php

namespace App\Http\Controllers;

use App\Enterprise;
use App\Http\Requests\EnterpriseRequest;
use Illuminate\Http\Request;

class EnterprisesController extends Controller
{
    public function index()
    {
        $enterprises = Enterprise::get();

        return view('enterprises.index', compact('enterprises'));
    }

    public function edit($enterprise)
    {
        return view('enterprises.edit', compact('enterprise'));
    }

    public function store(EnterpriseRequest $request)
    {
        $enterprise = new Enterprise($request->all());

        $enterprise->save();

        return redirect('enterprises');
    }

    public function update($enterprise, EnterpriseRequest $request)
    {
        $enterprise->update($request->all());

        return redirect('enterprises');
    }

    public function destroy($enterprise)
    {
        $enterprise->delete();

        return redirect('enterprises');
    }
}
