<?php

namespace App\Http\Controllers;

use App\Category;
use App\Enterprise;
use App\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $productsQuery = Product::query();
        if (request()->has('category')) {
            $productsQuery->byCategory(request()->input('category'));
        }
        if (request()->has('enterprise')) {
            $productsQuery->byEnterprise(request()->input('enterprise'));
        }

        $products = $productsQuery->simplePaginate(12);
        $categories = Category::all();
        $enterprises = Enterprise::all();

        return view('home', compact('products', 'categories', 'enterprises'));
    }
}
