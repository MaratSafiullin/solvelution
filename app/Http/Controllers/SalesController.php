<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SalesController extends Controller
{
    public function showProduct($product)
    {
        return view('sales.product-details', compact('product'));
    }
}
