<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\Category;
use App\Enterprise;

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;
    static $isAdmin;
    static $isRegistered;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'is_admin' => $isAdmin ?: 0,
        'is_registered' => $isRegistered ?: 1,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Category::class, function (Faker\Generator $faker) {
    return [
        'name' => ucwords($faker->unique()->word)
    ];
});

$factory->define(App\Enterprise::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->unique()->company,
        'description' => $faker->text(100)
    ];
});

$factory->define(App\Product::class, function (Faker\Generator $faker) {
    return [
        'category_id' => Category::all()->random()->id,
        'enterprise_id' => Enterprise::all()->random()->id,
        'name' => ucwords($faker->word . ' ' . $faker->word),
        'description' => $faker->text(100),
        'price' => $faker->randomFloat(2, 1, 100),
    ];
});

