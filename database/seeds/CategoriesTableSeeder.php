<?php

use App\Category;
use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Category::class)->create([
            'name' => 'Gifts'
        ]);
        factory(Category::class)->create([
            'name' => 'Skincare'
        ]);
        factory(Category::class)->create([
            'name' => 'Food&Wine'
        ]);
        factory(Category::class)->create([
            'name' => 'Clothing'
        ]);
        factory(Category::class)->create([
            'name' => 'Misc'
        ]);
    }
}
