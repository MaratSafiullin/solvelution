<?php

use App\Enterprise;
use Illuminate\Database\Seeder;

class EnterprisesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Enterprise::class)->create([
            'name' => 'Starbucks'
        ]);
        factory(Enterprise::class)->create([
            'name' => 'Warehouse'
        ]);
        factory(Enterprise::class)->create([
            'name' => 'Microsoft'
        ]);
        factory(Enterprise::class)->create([
            'name' => 'Panasonic'
        ]);
        factory(Enterprise::class)->create([
            'name' => 'Mercedes'
        ]);
    }
}
