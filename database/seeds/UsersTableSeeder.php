<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'name' => 'Marat',
            'email' => 'teenwoodsman@mail.ru',
            'is_admin' => 1
        ]);

        factory(User::class, 5)->create();

        factory(User::class, 10)->create([
            'is_registered' => 0,
        ])->each(function (User $user){
            $user->name = "Anonymous User {$user->id}";
            $user->email = "anonymous.user.{$user->id}@solvelution.nz";
            $user->save();
        });
    }
}
