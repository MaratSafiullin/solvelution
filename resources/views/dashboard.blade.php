@extends('layouts.app')

@section('content')
    <h1>Dashboard</h1>

    <hr/>

    <div class="well well-lg" style="line-height: 150%;">
        <h3>Products</h3>

        <div class="row form-group">
            <a href="/categories" class="btn btn-success">Categories</a>
            <a href="/enterprises" class="btn btn-success">Enterprises</a>
            <a href="/products" class="btn btn-success">Products</a>
        </div>
    </div>
@endsection
