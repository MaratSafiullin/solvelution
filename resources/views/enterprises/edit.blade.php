@extends('layouts.app')

@section('content')
    <h1>{{$enterprise->name}}</h1>

    <hr/>

    <div class="row">
        <div class="col-xs-2">
            <div class="form-group">
                <a href="/enterprises" class="btn btn-success">Back</a>
            </div>
        </div>
        <div class="col-xs-2 col-xs-offset-8">
            {!! Form::open(['method' => 'DELETE', 'route' => ['enterprises.destroy', $enterprise->id]]) !!}
            <div class="form-group">
                {!! Form::submit('Delete', ['class' => 'btn btn-danger form-control' ]) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>

    <div class="well well-lg text-center">
        {!! Form::model($enterprise, ['method' => 'PUT', 'route' => ['enterprises.update', $enterprise->id]]) !!}
        <div class="row form-group">
            <div class="col-xs-6">
                {!! Form::label('name', 'Enterprise name:') !!}
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder'=>'Enterprise name']) !!}
            </div>
            <div class="col-xs-6">
                {!! Form::label('integration_module', 'Integration module:') !!}
                {!! Form::text('integration_module', null, ['class' => 'form-control', 'placeholder'=>'Module name']) !!}
            </div>
        </div>
        <div class="row form-group">
            <div class="col-xs-12">
                {!! Form::label('description', 'Description:') !!}
                {!! Form::textarea('description', null,
                ['size' => '30x4', 'class' => 'form-control', 'placeholder'=>'Description']) !!}
            </div>
        </div>
        <div class="row">
            {!! Form::submit('Update', ['class' => 'btn btn-primary form-control']) !!}
        </div>
        {!! Form::close() !!}
    </div>


    @include('errors.list')
    </div>
@stop