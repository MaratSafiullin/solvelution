@extends('layouts.app')

@section('content')

    <div class="homepage-menu">
        <div class="row">
            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                <a class="navbar-brand products-link dropdown-toggle" data-toggle="dropdown"
                   aria-haspopup="true" aria-expanded="true">Impact Seller <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="/?category={{request()->input('category')}}">
                            All
                        </a>
                    </li>
                    @foreach($enterprises as $enterprise)
                        <li>
                            <a href="/?category={{request()->input('category')}}&enterprise={{$enterprise->id}}">
                                {{$enterprise->name}}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
            @foreach($categories as $category)
                <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                    <a class="navbar-brand products-link"
                       href="/?category={{$category->id}}&enterprise={{request()->input('enterprise')}}">
                        {{$category->name}}
                    </a>
                </div>
            @endforeach
        </div>
    </div>

    <table class="products-list">
        <tr>
            <td class="prev-page-button">
                {{ $products->appends(request()->except('page'))->links() }}
            </td>
            <td class="products-list-content">
                <div class="row" style="background-color: lightgrey;">
                    @foreach ($products as $product)
                        <div class="col-sm-6 col-md-4 col-lg-3">
                            <a href="/product-details/{{$product->id}}">
                                <div style="background-color: white; margin: 5px 0; text-align: center">
                                    <img src="" width="200" height="200"
                                         alt="IMAGE FOR: {{$product->name}}
                                                 __________________________
                                                 CAT: {{$product->category->name}}
                                                 __________________________
                                                 ENT: {{$product->enterprise->name}}"
                                    >
                                    <br/>
                                    <b>{{$product->name}}</b>
                                    <br/>
                                    <h3>${{$product->price}}</h3>
                                    <hr style="margin: 5px 20px; border-color: black"/>
                                    {{$product->enterprise->name}}
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            </td>
            <td class="next-page-button">
                {{ $products->appends(request()->except('page'))->links() }}
            </td>
        </tr>
    </table>

@endsection