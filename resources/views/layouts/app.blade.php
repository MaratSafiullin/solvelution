<!DOCTYPE html>
<html lang="en">
<head>
  @include('layouts.partials.head')
  @include('layouts.partials.head-scripts')
</head>
<body>
  <header class="header">
    @include('layouts.partials.header.top-utility-bar')
    @include('layouts.partials.header.main-menu-bar')
  </header>
  <main class="main">
    <div class="container">
      @yield('content')
    </div>
  </main>
  <footer class="footer">
    @include('layouts.partials.footer')
  </footer>
  @include('layouts.partials.footer-links')
</body>
</html>
