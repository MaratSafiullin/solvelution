<div class="main-menu-bar-wrap">
  <div class="container">
    <nav class="main-menu-bar navbar">
      <div class="navbar-header">
        <button type="button" data-toggle="collapse" data-target="#main-menu-bar-navbar-collapse" aria-expanded="false" class="navbar-toggle collapsed">
          <span class="menu-txt">Main Menu</span>
          <span class="icon-grp">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </span>
        </button>
        <a class="navbar-brand products-link hidden-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Products <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <li><a href="/">All</a></li>
          @foreach($categories as $category)
            <li><a href="/?category={{$category->id}}">{{$category->name}}</a></li>
          @endforeach
        </ul>
        <a href="{{ url('/') }}" class="navbar-brand logo hidden-sm hidden-md hidden-lg">{{ config('app.name', 'Home') }}</a>
      </div>
      <div id="main-menu-bar-navbar-collapse" class="collapse navbar-collapse">
        <ul class="nav navbar-nav">
          <li class="hidden-sm hidden-md hidden-lg"><a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Products <span class="caret"></span></a>
            <ul class="dropdown-menu">
              <li><a href="/">All</a></li>
              @foreach($categories as $category)
                <li><a href="/?category={{$category->id}}">{{$category->name}}</a></li>
              @endforeach
            </ul>
          </li>
          <li><a href="#">SE Sellers</a></li>
          <li><a href="#">Local Causes</a></li>
          <li class="logo active hidden-xs"><a href="{{ url('/') }}"><span>{{ config('app.name', 'Home') }}</span></a></li>
          <li><a href="#">How It Works</a></li>
          <li><a href="#">Contact Us</a></li>
        </ul>
      </div>
    </nav>
  </div>
</div>
