<nav class="top-utility-bar navbar">
  <div class="container">
    <div class="navbar-header">
      <button type="button" data-toggle="collapse" data-target="#utility-bar-navbar-collapse" aria-expanded="false" class="navbar-toggle collapsed">
        <span class="icon-grp">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </span>
        <span class="menu-txt">User Menu</span>
      </button>
      <a href="#" class="my-local-causes-link"><span>My Local Causes</span></a>
    </div>
    <div id="utility-bar-navbar-collapse" class="collapse navbar-collapse">
      <div class="top-utility-bar__right-items">
        <div class="top-utility-bar__search">
          <div class="input-group">
            <div class="input-group-btn">
              <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-default dropdown-toggle">
                <span class="caret"></span>
              </button>
              <div class="top-utility-bar__adv-search dropdown-menu">
                <form id="frmAdvSearch" role="form" class="form-horizontal">
                  <div class="form-group">
                    <label for="filterSearch">Filter by</label>
                    <select name="filterSearch" id="filterSearch" class="form-control">
                      <option value="0" selected="">All filters</option>
                      <option value="1">Featured</option>
                      <option value="2">Most popular</option>
                      <option value="3">Top rated</option>
                      <option value="4">Most commented</option>
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="contain">Seller</label>
                    <input type="text" class="form-control"/>
                  </div>
                  <div class="form-group">
                    <label for="contain">Contains the words</label>
                    <input type="text" class="form-control"/>
                  </div>
                  <div class="form-group">
                    <button type="submit" class="btn btn-primary">
                      <span aria-hidden="true" class="glyphicon glyphicon-search"></span>
                    </button>
                  </div>
                </form>
              </div>
            </div>
            <input type="text" placeholder="Search" class="form-control"/>
            <span role="group" class="input-group-btn">
              <button type="button" class="btn btn-default">
                <span aria-hidden="true" class="glyphicon glyphicon-search"></span>
              </button>
            </span>
          </div>
        </div>
        <!-- / top-utility-bar__search-->
        <ul class="nav navbar-nav navbar-right">
          <li><a href="#"><span aria-hidden="true" class="glyphicon glyphicon-star-empty"></span> Wishlist</a></li>
          <li><a href="#"><span aria-hidden="true" class="glyphicon glyphicon-shopping-cart"></span> Cart</a></li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
              <span aria-hidden="true" class="glyphicon glyphicon-user"></span>
              @if (Auth::user()->isAnonymous())
                User
              @else
                {{ Auth::user()->name }}
              @endif
              <span class="caret"></span>
            </a>
            <ul class="dropdown-menu" role="menu">
              <!-- Authentication Links -->
              @if (Auth::user()->isAnonymous())
                <li><a href="{{ url('/login') }}">Login</a></li>
                <li><a href="{{ url('/register') }}">Register</a></li>
              @else
                <li><a href="{{ url('/dashboard') }}">Dashboard</a></li>
                <li>
                  <a href="{{ url('/logout') }}"
                     onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                    Logout
                  </a>

                  <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                    {{ csrf_field() }}
                  </form>
                </li>
                {{--<li class="dropdown">--}}
                    {{--<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">--}}
                        {{--{{ Auth::user()->name }} <span class="caret"></span>--}}
                    {{--</a>--}}

                    {{--<ul class="dropdown-menu" role="menu">--}}
                        {{--<li>--}}
                            {{--<a href="{{ url('/logout') }}"--}}
                               {{--onclick="event.preventDefault();document.getElementById('logout-form').submit();">--}}
                                {{--Logout--}}
                            {{--</a>--}}

                            {{--<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">--}}
                                {{--{{ csrf_field() }}--}}
                            {{--</form>--}}
                        {{--</li>--}}
                    {{--</ul>--}}
                {{--</li>--}}
              @endif
            </ul>
          </li>
        </ul>
      </div>
    </div>
  </div>
</nav>
