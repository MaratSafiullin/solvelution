@extends('layouts.app')

@section('content')
    <h1>{{$product->name}}</h1>

    <hr/>

    <div class="row">
        <div class="col-xs-2">
            <div class="form-group">
                <a href="/products" class="btn btn-success">Back</a>
            </div>
        </div>
        <div class="col-xs-2 col-xs-offset-8">
            {!! Form::open(['method' => 'DELETE', 'route' => ['products.destroy', $product->id]]) !!}
            <div class="form-group">
                {!! Form::submit('Delete', ['class' => 'btn btn-danger form-control' ]) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>

    <div class="well well-lg text-center">
        {!! Form::model($product, ['method' => 'PUT', 'route' => ['products.update', $product->id]]) !!}
        @include('products._form', ['submitButtonText' => 'Update'])
        {!! Form::close() !!}

        @include('errors.list')
    </div>
@stop