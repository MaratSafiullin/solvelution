@extends('layouts.app')

@section('content')
    <div class="product-details">
        <div class="row">
            <div class="col-xs-8">
                <h1>{{$product->name}}</h1>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8">
                <img class="product-image"
                     alt="IMAGE FOR: {{$product->name}}
                             -
                             CAT: {{$product->category->name}}
                             -
                             ENT: {{$product->enterprise->name}}"
                >
            </div>
            <div class="col-md-4">
                <h1 class="product-price">${{$product->price}}</h1>
                <hr/>
                <div>
                    {{$product->description}}
                </div>
                <hr/>
                <div>
                    <div class="form-group">
                        Quantity {!! Form::text('quantity', 0, ['class' => '']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::submit('Add to Cart', ['class' => 'btn btn-primary form-control']) !!}
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

        </div>
    </div>

@endsection