@if ($paginator->hasPages())
    <ul class="pagination">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li class="prev-page disabled"><span>&laquo;</span></li>
        @else
            <li class="prev-page"><a href="{{ $paginator->previousPageUrl() }}" rel="prev">&laquo;</a></li>
        @endif

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li class="next-page"><a href="{{ $paginator->nextPageUrl() }}" rel="next">&raquo;</a></li>
        @else
            <li class="next-page disabled"><span>&raquo;</span></li>
        @endif
    </ul>
    @if ($paginator->onFirstPage())
        <button class="prev-page disabled hidden"><span>&laquo;</span></button>
    @else
        <a href="{{ $paginator->previousPageUrl() }}" rel="prev">
            <button class="prev-page hidden">&laquo;</button>
        </a>
    @endif
    @if ($paginator->hasMorePages())
        <a href="{{ $paginator->nextPageUrl() }}" rel="next">
            <button class="next-page hidden">&raquo;</button>
        </a>
    @else
        <button class="next-page disabled hidden"><span>&raquo;</span></button>
    @endif
@endif
