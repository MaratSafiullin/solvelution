<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::get('dashboard', 'DashboardController@index');
Route::get('product-details/{product}', 'SalesController@showProduct');

Route::resource('categories', 'CategoriesController');
Route::resource('enterprises', 'EnterprisesController');
Route::resource('products', 'ProductsController');

